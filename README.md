PASOS PARA INSTALAR Y EJECUTAR EL PROYECTO:

•	Instalar NodeJs Versión 18.0

•	Instalar PostgreSQL Versión 14.2

•	Clonar repositorio:

git clone https://gitlab.com/edgardopanchana/comidapp-backend.git 

•	Situarse en carpeta de Proyecto

Cd backend-comidapp

•	Para la generación de la base de datos a partir del backend
Ir al archivo “server.js” y verificar que la siguiente línea de código esté descomentada:
db.sequelize.sync({force:true, logging:false}).then(()=>{
initial();
})
La propiedad force en true permite la migración de la base de datos y elimina los registros de las tablas ingresados previamente.
En el archivo config/db_config.js se deben cambiar los valores del servidor de base de datos.

•	Instalar dependencias:

npm install

•	Ejecutar Proyecto y ejecutar migraciones

npm run server.js

•	Ejecutar Proyecto con Nodemon (Opcional)

npm install –g nodemon

nodemon server.js

•	Test de proyecto:

Abrir el navegador web e ir a: http://localhost:8080
Se podrá observar el siguiente mensaje:
{"message":"Server is ready."}


La versión del servidor en producción se encuentra en la siguiente dirección:
http://137.184.106.160:8080/api
Se podrá observar el siguiente mensaje:
{"message":"Server is ready."}

