const { authJwt } = require("../middleware");
const reservationController = require("../controllers/reservation.controller");
const verifyReservation = require("../middleware/verifyReservation");

module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });
  app.get('/api/reservationsByUser/:userId', [authJwt.verifyToken], reservationController.getReservationsByUser)
  app.post(
    "/api/reservations/:eventId",
    [authJwt.verifyToken, verifyReservation.reservationRules(), verifyReservation.validateReservation],
    reservationController.createReservation
    );
    app.get('/api/getReservationsByChef/:userId', [authJwt.verifyToken], reservationController.getReservationsByChef)
};
