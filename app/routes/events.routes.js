const { authJwt } = require("../middleware");
const eventsController = require("../controllers/events.controller");
//const verifyFood = require('../middleware/verifyFood')

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });
  
  app.get('/api/getEventByFood/:id', authJwt.verifyToken, eventsController.getEvents)
  app.get('/api/getEventById/:id', authJwt.verifyToken, eventsController.getEvent)
  app.post(
    "/api/events",
    [ 
      authJwt.verifyToken,
      //verifyFood.foodRules(),
      //verifyFood.validateFood,
    ],
    eventsController.createEvent
  );
};