const config = require("../config/db_config.js");
const Sequelize = require("sequelize");

const sequelize = new Sequelize(
  config.DB,
  config.USER,
  config.PASSWORD,
  {
    host: config.HOST,
    dialect: config.dialect,
    operatorsAliases: false,
    pool: {
      max: config.pool.max,
      min: config.pool.min,
      acquire: config.pool.acquire,
      idle: config.pool.idle
    }
  }
);

const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;
db.user = require("./user.js")(sequelize, Sequelize);
db.role = require("./roles.js")(sequelize, Sequelize);
db.food = require("./food.js")(sequelize, Sequelize);
db.events = require("./events.js")(sequelize, Sequelize);
db.foodImage =  require('./foodImage.js')(sequelize, Sequelize)
db.reservations = require("./reservation.js")(sequelize, Sequelize);
db.role.belongsToMany(db.user, {
  through: "user_roles",
  foreignKey: "id_rol",
  otherKey: "id_user"
});
db.user.belongsToMany(db.role, {
  through: "user_roles",
  foreignKey: "id_user",
  otherKey: "id_rol"
});

db.user.hasMany(db.food);
db.food.belongsTo(db.user);

db.food.hasMany(db.foodImage);
db.foodImage.belongsTo(db.food);

db.food.hasMany(db.events);
db.events.belongsTo(db.food);

db.user.hasMany(db.reservations);
db.reservations.belongsTo(db.user);

db.events.hasMany(db.reservations);
db.reservations.belongsTo(db.events);


db.ROLES = ["customer", "sales"];
module.exports = db;