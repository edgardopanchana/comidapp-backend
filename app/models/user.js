module.exports = (sequelize, Sequelize) => {
    const User = sequelize.define("users", {
      email: {
        type: Sequelize.STRING
      },
      password: {
        type: Sequelize.STRING
      },
      name: {
        type: Sequelize.STRING
      },
      last_name: {
        type: Sequelize.STRING
      },
      id_rol: {
        type: Sequelize.STRING
      },
      id_language: {
        type: Sequelize.STRING
      },
      about_me: {
        type: Sequelize.STRING
      },
      latitude: {
        type: Sequelize.STRING
      },
      longitude: {
        type: Sequelize.STRING
      },
      photo: {
        type: Sequelize.STRING
      },
      direction: {
        type: Sequelize.STRING
      },
    });
    return User;
  };