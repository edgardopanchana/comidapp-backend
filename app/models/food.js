module.exports = (sequelize, Sequelize) => {
    const Food = sequelize.define("foods", {
      name: {
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.STRING
      },
      pvp: {
        type: Sequelize.DECIMAL(10, 2)
      },
      latitude: {
        type: Sequelize.STRING
      },
      longitude: {
        type: Sequelize.STRING
      },
      likes: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
      },
      active: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true,
      },
      city: {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: ''
      },
      image:{
        type: Sequelize.STRING,
      },
      max_tourist: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 1
      }
    });
    return Food;
  };