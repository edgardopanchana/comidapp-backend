module.exports = (sequelize, Sequelize) => {
  const Reservation = sequelize.define("reservations", {
    persons: {
      type: Sequelize.INTEGER,
    },
    paid: {
      type: Sequelize.BOOLEAN,
    },
    message: {
      type: Sequelize.STRING,
    },
    active: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    },
  });
  return Reservation;
};
