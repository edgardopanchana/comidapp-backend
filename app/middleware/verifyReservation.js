const { body, validationResult } = require('express-validator');
const db = require("../models");
const Reservation = db.reservations;

// main validation middleware
validateReservation = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty())
    return res.status(422).json({ message: errors.array()[0].msg });
  return next();
};

// rules validation
reservationRules = () => {
  return [
    body("message")
      .trim()
      .notEmpty()
      .withMessage("Mensaje requerido")
      .isLength({ max: 200 })
      .withMessage("El campo nombre no puede superar los 200 caracteres"),
    body("persons")
      .trim()
      .notEmpty()
      .withMessage("Número de personas requerido")
      .isNumeric()
      .withMessage("El campo número de personas debe ser numérico"),
  ];
};

const verifyReservation = {
  validateReservation,
  reservationRules
};

module.exports = verifyReservation;
