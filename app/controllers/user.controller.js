const db = require("../models");
const User = db.user;
const Op = db.Sequelize.Op;

exports.getUser = async (req, res) => {
  try {
    const { id } = req.params;
    const user = await User.findOne({ where: { id: id } });
    if (user){
      return res.status(200).send({ status: true, user: user });
    }else{
      return res.status(200).send({ status: true, message: 'No existe ningun usuario con ese ID' });
    }
  } catch (error) {
    console.log(error.message);
    res.status(500).send({ status: false, message: "¡Ha ocurrido un error!" });
  }
};
