const db = require("../models");
const config = require("../config/auth_config");
const User = db.user;
const Role = db.role;
const Op = db.Sequelize.Op;
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

exports.signup = (req, res) => {
  const {
    email,
    password,
    name,
    last_name,
    id_rol,
    id_language,
    about_me,
    latitude,
    longitude,
    photo,
    direction,
  } = req.body;

  User.create({
    email,
    password: bcrypt.hashSync(password, 8),
    name,
    last_name,
    id_rol,
    id_language,
    about_me,
    latitude,
    longitude,
    photo,
    direction,
  })
    .then((user) => {
      if (req.body.roles) {
        Role.findAll({
          where: {
            description: {
              [Op.or]: req.body.roles,
            },
          },
        }).then((roles) => {
          user.setRoles(roles).then(() => {
            return res
              .status(201)
              .send({ status: true, message: "¡Usuario registrado correctamente!" });
          });
        });
      } else {
        // user role = 1
        user.setRoles([1]).then(() => {
          return res
            .status(201)
            .send({ status: true, message: "¡Usuario registrado correctamente!" });
        });
      }
    })
    .catch((err) => {
      res.status(500).send({ status: false, message: 'Ha ocurrido un error' });
    });
};

exports.signin = (req, res) => {
  const user = req.user; // user found
  const passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
  if (!passwordIsValid) {
    return res
      .status(401)
      .send({ accessToken: null, message: "Contraseña incorrecta" });
  }
  const { id, name, last_name, email, photo } = user;
  const token = jwt.sign({ id }, config.secret, { expiresIn: 86400 });
  let authorities = [];
  user
    .getRoles()
    .then((roles) => {
      for (let i = 0; i < roles.length; i++) {
        authorities.push("ROLE_" + roles[i].description.toUpperCase());
      }
      res.status(200).send({
        id,
        name,
        last_name,
        email,
        photo,
        roles: authorities,
        accessToken: token,
      });
    })
    .catch((err) => {
      res.status(500).send({ status: false, message: 'Ha ocurrido un error' });
    });
};
