const db = require("../models");
const config = require("../config/auth_config");
const User = db.user;
const Role = db.role;
const Events = db.events;
const Op = db.Sequelize.Op;

//Crear eventos
exports.createEvent = async (req, res) => {
  try {
    const { foodId, date, hour } = req.body;
    const result = await Events.create({
        foodId,
        date,
        hour,
    });
    return res
      .status(201)
      .send({ status: true, message: "Evento registrado correctamente!" });
  } catch (error) {
    console.log(error);
    res.status(500).send({ status: false, message: "¡Ha ocurrido un error!" });
  }
};

//Obtener eventos por id de comida
exports.getEvents = async (req, res) => {
  try {
    const { id } = req.params;
    const event = await Events.findAll({ where: { foodId: id, estado: true  } });
    if (event){
      return res.status(200).send({ status: true, events: event });
    }else{
      return res.status(200).send({ status: true, message: 'No existe ningun evento de esa comida' });
    }
  } catch (error) {
    console.log(error.message);
    res.status(500).send({ status: false, message: "¡Ha ocurrido un error!" });
  }
};

//Obtener evento por id de evento
exports.getEvent = async (req, res) => {
  try {
    const { id } = req.params;
    const event = await Events.findOne({ where: { id: id, estado: true } });
    if (event){
      return res.status(200).send({ status: true, event: event });
    }else{
      return res.status(200).send({ status: true, message: 'No existe ningun evento con ese ID' });
    }
  } catch (error) {
    console.log(error.message);
    res.status(500).send({ status: false, message: "¡Ha ocurrido un error!" });
  }
};
