const db = require("../models");
const config = require("../config/auth_config");
const User = db.user;
const Event = db.events;
const Food = db.food;
const Reservation = db.reservations;

//Crear reserva
exports.createReservation = async (req, res) => {
  try {
    const { message, persons } = req.body;
    const result = await Reservation.create({
      message,
      persons,
      paid: true,
      eventId: req.params.eventId,
      userId: req.userId,
    });
    return res
      .status(201)
      .send({ status: true, message: "Reserva realizada!" });
  } catch (error) {
    console.log(error);
    res.status(500).send({ status: false, message: "¡Ha ocurrido un error!" });
  }
};

//Obtener reservas por usuario reservador
exports.getReservationsByUser = async (req, res) => {
  try {
    const { userId } = req.params;
    const reservations = await Reservation.findAll({
      where: { userId },
    });
    return res.status(200).send({ status: true, reservations });
  } catch (error) {
    console.log(error);
    res.status(500).send({ status: false, message: "¡Ha ocurrido un error!" });
  }
};


//Obtener reservas por usuario reservador
exports.getReservationsByChef = async (req, res) => {
  try {
    const { userId } = req.params;
    // const foods = await Food.findAll({
    //   where: {userId: userId},
    // })
    // let events = []
    // for await (const food of foods){
    //   events = await Event.findAll({
    //     where: { foodId: food.id },
    //   });
    // }
    // let reservations = []
    // for await (const event of events){
    //   reservations = await Reservation.findAll({
    //     where: { eventId: event.id },
    //   });
    // }
    const reservations = await Food.findAll({ where: { userId }, 
      include:[
        {association: Event}
      ]
  });
    return res.status(200).send({ status: true, reservations });
  } catch (error) {
    console.log(error);
    res.status(500).send({ status: false, message: "¡Ha ocurrido un error!" });
  }
};