module.exports = {
    HOST: "localhost",//Cambiar a los datos del nombre del servidor
    USER: "postgres",//Cambiar a los datos del usuario del servidor
    PASSWORD: "xxxxxxxx", //Cambiar a contraseña del servidor
    DB: "comidapp",
    dialect: "postgres",
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
  };